package sso.demo.lib.services.impl;

import sso.demo.lib.services.UserService;
import org.springframework.security.core.context.SecurityContextHolder;

public class UserServiceImpl implements UserService {

    @Override
    public long getUserId() {
        return Long.parseLong(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
    }
}
