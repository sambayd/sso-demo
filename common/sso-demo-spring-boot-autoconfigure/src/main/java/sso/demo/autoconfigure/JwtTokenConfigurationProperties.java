package sso.demo.autoconfigure;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;

@ConfigurationProperties("sso.demo.jwt")
public class JwtTokenConfigurationProperties {

    @Getter
    private boolean authServer;

    @Getter
    @Setter
    private AuthorizationServerProperties auth = new AuthorizationServerProperties();

    @Getter
    @Setter
    private ResourceServerProperties resource = new ResourceServerProperties();

    @PostConstruct
    private void init() {
        if (auth.jksFile != null)
            authServer = true;
    }

    @Getter
    @Setter
    public static class AuthorizationServerProperties {

        private String jksFile;

        private String jksPassword;

        private String pairName;
    }

    @Getter
    @Setter
    public static class ResourceServerProperties {

        private String publicKeyFile;
    }
}
