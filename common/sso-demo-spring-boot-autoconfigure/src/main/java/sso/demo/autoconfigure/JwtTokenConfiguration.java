package sso.demo.autoconfigure;

import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableConfigurationProperties(JwtTokenConfigurationProperties.class)
@RequiredArgsConstructor
public class JwtTokenConfiguration {

    private final JwtTokenConfigurationProperties properties;

    @Bean
    public TokenStore jwtTokenStore(JwtAccessTokenConverter jwtAccessTokenConverter) {
        return new JwtTokenStore(jwtAccessTokenConverter);
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        final JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();

        if (properties.isAuthServer()) {
            final KeyStoreKeyFactory factory = new KeyStoreKeyFactory(new ClassPathResource(
                    properties.getAuth().getJksFile()), properties.getAuth().getJksPassword().toCharArray());
            jwtAccessTokenConverter.setKeyPair(factory.getKeyPair(properties.getAuth().getPairName()));
        } else {
            try {
                jwtAccessTokenConverter.setVerifierKey(IOUtils.toString(new ClassPathResource(
                        properties.getResource().getPublicKeyFile()).getInputStream(), Charset.forName("utf-8")));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return jwtAccessTokenConverter;
    }

    @Bean
    public DefaultTokenServices defaultTokenServices(TokenStore tokenStore, TokenEnhancer tokenEnhancer) {
        final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore);
        defaultTokenServices.setTokenEnhancer(tokenEnhancer);
        defaultTokenServices.setAccessTokenValiditySeconds((int) TimeUnit.DAYS.convert(2, TimeUnit.SECONDS));
        defaultTokenServices.setRefreshTokenValiditySeconds((int) TimeUnit.DAYS.convert(4, TimeUnit.SECONDS));
        defaultTokenServices.setSupportRefreshToken(true);
        return defaultTokenServices;
    }
}

