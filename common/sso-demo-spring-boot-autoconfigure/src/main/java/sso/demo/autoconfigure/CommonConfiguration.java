package sso.demo.autoconfigure;

import sso.demo.lib.services.UserService;
import sso.demo.lib.services.impl.UserServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommonConfiguration {

    @Bean
    public UserService userService() {
        return new UserServiceImpl();
    }
}
