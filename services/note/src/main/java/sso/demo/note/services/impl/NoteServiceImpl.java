package sso.demo.note.services.impl;

import sso.demo.lib.services.UserService;
import sso.demo.note.data.jpa.NoteRepository;
import sso.demo.note.exceptions.NoteDoestNotExistsException;
import sso.demo.note.model.entities.Note;
import sso.demo.note.services.NoteService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collection;

@Service
@RequiredArgsConstructor
public class NoteServiceImpl implements NoteService {

    private final NoteRepository noteRepository;

    private final UserService userService;

    @Override
    public long createUserNote(Note note) {
        note.setId(null);
        note.setUserId(userService.getUserId());
        return noteRepository.save(note).getId();
    }

    @Override
    public Note getUserNote(long noteId) throws NoteDoestNotExistsException {
        return noteRepository.findByIdAndUserId(noteId, userService.getUserId())
                .orElseThrow(NoteDoestNotExistsException::new);
    }

    @Override
    public Collection<Note> getUserNotes() {
        return noteRepository.findAllByUserId(userService.getUserId());
    }

    @Override
    public void updateUserNote(Note note) throws NoteDoestNotExistsException {
        note.setUserId(userService.getUserId());
        note.setLastEdited(Timestamp.valueOf(LocalDateTime.now()));
        if (noteRepository.existsByIdAndUserId(note.getId(), note.getUserId()))
            noteRepository.save(note);
        else
            throw new NoteDoestNotExistsException();
    }

    @Override
    public void deleteUserNote(long id) throws NoteDoestNotExistsException {
        if (noteRepository.existsByIdAndUserId(id, userService.getUserId()))
            noteRepository.deleteById(id);
        else
            throw new NoteDoestNotExistsException();
    }
}
