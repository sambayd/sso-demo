package sso.demo.note.controllers;

import sso.demo.note.exceptions.NoteDoestNotExistsException;
import sso.demo.note.model.entities.Note;
import sso.demo.note.services.NoteService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/current-user/note")
@RequiredArgsConstructor
@PreAuthorize("hasRole('USER')")
public class CurrentUserNoteController {

    private final NoteService noteService;

    @PostMapping
    public ResponseEntity<Long> createNote(@RequestBody Note note) {
        return ok(noteService.createUserNote(note));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Note> getNote(@PathVariable("id") long id) {
        try {
            return ok(noteService.getUserNote(id));
        } catch (NoteDoestNotExistsException e) {
            return notFound().build();
        }
    }

    @GetMapping
    public ResponseEntity<Collection<Note>> getNotes() {
        return ok(noteService.getUserNotes());
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateNote(@PathVariable("id") long id, @RequestBody Note note) {
        try {
            note.setId(id);
            noteService.updateUserNote(note);
            return ok().build();
        } catch (NoteDoestNotExistsException e) {
            return notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteNote(@PathVariable("id") long id) {
        try {
            noteService.deleteUserNote(id);
            return ok().build();
        } catch (NoteDoestNotExistsException e) {
            return notFound().build();
        }
    }
}
