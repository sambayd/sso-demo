package sso.demo.note.services;

import sso.demo.note.exceptions.NoteDoestNotExistsException;
import sso.demo.note.model.entities.Note;

import java.util.Collection;

public interface NoteService {

    long createUserNote(Note note);

    Note getUserNote(long noteId) throws NoteDoestNotExistsException;

    Collection<Note> getUserNotes();

    void updateUserNote(Note note) throws NoteDoestNotExistsException;

    void deleteUserNote(long id) throws NoteDoestNotExistsException;
}
