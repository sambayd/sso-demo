package sso.demo.note.data.jpa;

import sso.demo.note.model.entities.Note;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;

import java.util.Collection;
import java.util.Optional;

public interface NoteRepository extends CrudRepository<Note, Long> {

    Optional<Note> findByIdAndUserId(long id, long userId);

    @NonNull
    Collection<Note> findAllByUserId(long userId);

    boolean existsByIdAndUserId(long id, long userId);
}
