var app;
var baseUrn = '/note-service/api/current-user/note/';

updateTokenIfNeed(function () {
    setupGlobalAuthHeader();

    axios.interceptors.response.use(null, function (error) {
        if (error.config && error.response && error.response.status === 401) {
            return new Promise(function (resolve, reject) {
                updateToken(function () {
                    error.config.headers["Authorization"] = setupGlobalAuthHeader();
                    axios.request(error.config).then(resolve, reject);
                });
            })
        }
        return Promise.reject(error);
    });


    Vue.component('note', {
        props: ['note'],
        template:
        "<div>" +
        "<span>Name: </span><input class='form-control' type='text' v-model='note.name' v-on:input='updateNote'><br>" +
        "<textarea class='form-control' v-model='note.text' v-on:input='updateNote'>{{ note.text }}</textarea>" +
        "<p>Last edited by {{ lastEdited }}</p>" +
        "<button class='btn' v-on:click='deleteNote'>Delete note</button>" +
        "</div>",

        computed: {
            lastEdited() {
                var groups = /([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2})/
                    .exec(this.note.lastEdited);
                return !groups ? "" : groups[3] + "." + groups[2] + "." + groups[1] + " at " +
                    groups[4] + ":" + groups[5] + ":" + groups[6]
            }
        },

        methods: {
            deleteNote() {
                axios.delete(baseUrn + this.note.id)
                    .then(() => this.$emit('deleteNote', this.note.id))
            },

            updateNote() {
                axios.put(baseUrn + this.note.id, this.note)
                    .then(() => {
                        axios.get(baseUrn + this.note.id)
                            .then(response => this.note.lastEdited = response.data.lastEdited)
                    })
            }
        }
    });

    app = new Vue({
        el: "#app",
        data: {
            notes: []
        },

        mounted() {
            axios.get(baseUrn)
                .then(response => this.notes = response.data)
        },

        template:
        "<div>" +
        "<note v-for='note in notes' :key='note.id' :note='note' v-on:deleteNote='deleteNoteById'></note>" +
        "<br><button class='btn' v-on:click='createNewNote'>Create new note</button>" +
        "</div>",

        methods: {
            createNewNote() {
                axios.post(baseUrn, {})
                    .then(response => {
                        axios.get(baseUrn + response.data)
                            .then(response => this.notes.push(response.data))
                    })
            },

            deleteNoteById(id) {
                var el = this.notes.find(note => note.id === id);
                this.notes.splice(this.notes.indexOf(el), 1);
            }
        }
    });

});

function setupGlobalAuthHeader() {
    var header = "Bearer " + getAccessToken();
    axios.defaults.headers.common["Authorization"] = header;
    return header;
}
