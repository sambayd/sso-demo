function logout(action) {
    axios.post("/logout")
        .then(function () {
            Cookies.remove("ACCESS-TOKEN");
            Cookies.remove("REFRESH-TOKEN");
            action();
        });
}

function logoutAndRedirectToHome() {
    logout(function () {
        window.location.replace("/")
    });
}

logoutAndRedirectToHome();
