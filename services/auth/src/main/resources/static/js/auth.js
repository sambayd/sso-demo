var client_id = "external";
var client_secret = "password";

var axiosTokenInstance = axios.create();
axiosTokenInstance.defaults.headers.post['Content-Type'] = "application/x-www-form-urlencoded";

function getAccessToken() {
    return Cookies.get("ACCESS-TOKEN");
}

function getRefreshToken() {
    return Cookies.get("REFRESH-TOKEN")
}

function saveToken(data) {
    Cookies.set("ACCESS-TOKEN", data.access_token);
    Cookies.set("REFRESH-TOKEN", data.refresh_token);
}

function updateTokenIfNeed(success, failure) {
    if (getAccessToken() === undefined)
        updateToken(success, failure);
    else
        validateAccessToken(success, function () {
            updateToken(success, failure);
        });
}

function updateToken(success, failure) {
    var refreshToken = getRefreshToken();
    var newSuccess = function (response) {
        saveToken(response.data);
        success();
    };
    if (refreshToken !== undefined)
        updateTokenByRefreshToken(refreshToken, newSuccess, failure);
    else
        updateTokenByAuthorizationCode(newSuccess, failure);
}

function updateTokenByRefreshToken(refreshToken, success, failure) {
    getTokenByRefreshToken(refreshToken, success,
        function (error) {
            if (error.response.data.error === "invalid_token") {
                updateTokenByAuthorizationCode(success, failure);
            } else {
                failure();
            }
        })
}

function getTokenByRefreshToken(refreshToken, success, failure) {
    getToken("grant_type=refresh_token&refresh_token=" + refreshToken, success, failure)
}

function getTokenByAuthorizationCode(code, redirectUri, success, failure) {
    getToken("grant_type=authorization_code&code=" + code + "&redirect_uri=" + redirectUri, success, failure)
}

function getToken(body, success, failure) {
    axiosTokenInstance.post("/oauth/token", body, {
        headers: {
            Authorization: "Basic " + btoa(client_id + ":" + client_secret)
        }
    })
        .then(success)
        .catch(failure);
}

function updateTokenByAuthorizationCode(success, failure) {
    var redirectUri= window.location.pathname;
    var code = getAuthorizationCode(redirectUri);

    if (code != null)
        getTokenByAuthorizationCode(code, redirectUri, success, function (error) {
            if (error.response.data.error === "invalid_grant")
                redirectToAuthCodePage(redirectUri);
            else
                failure();
        });
}

function getAuthorizationCode(redirectUri) {
    var url = new URL(window.location.href);
    var code = url.searchParams.get("code");

    if (code == null) {
        redirectToAuthCodePage(redirectUri);
        return null;
    }
    return code;
}

function redirectToAuthCodePage(redirectUri) {
    var param = "client_id=" + client_id + "&response_type=code&redirect_uri=" + redirectUri;
    window.location.replace("/oauth/authorize?" + param);
}

function validateAccessToken(success, failure) {
    axiosTokenInstance.post("/oauth/check_token", "token=" + getAccessToken())
        .then(success)
        .catch(failure);
}


function confirmLoginInsteadRegistration() {
    confirmInstead("/confirm-login")
}

function confirmRegistrationInsteadLogin() {
    confirmInstead("/confirm-registration")
}

function confirmInstead(urn) {
    axios.post(urn)
        .then(function () {
            window.location.replace("/");
        });
}
