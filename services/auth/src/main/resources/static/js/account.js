var app;
var baseUrn = '/account-service/api/account/current';

updateTokenIfNeed(function () {
    setupGlobalAuthHeader();

    axios.interceptors.response.use(null, function (error) {
        if (error.config && error.response && error.response.status === 401) {
            return new Promise(function (resolve, reject) {
                updateToken(function () {
                    error.config.headers["Authorization"] = setupGlobalAuthHeader();
                    axios.request(error.config).then(resolve, reject);
                });
            })
        }
        return Promise.reject(error);
    });

    app = new Vue({
        el: "#app",
        data: {
            account: {}
        },

        mounted() {
            axios.get(baseUrn)
                .then(response => {
                    this.account = response.data;
                    console.log(response.data);
                })
        },

        template:
        "<div>" +
        "<div>Fist Name: <input class='form-control' v-model='account.firstName' v-on:input='updateAccount()'></div>" +
        "<div>Last Name: <input class='form-control' v-model='account.lastName' v-on:input='updateAccount()'></div>" +
        "<div>About me: <textarea class='form-control' v-model='account.aboutMe' v-on:input='updateAccount()'></textarea></div>" +
        "</div>",

        methods: {
            updateAccount() {
                axios.put(baseUrn, this.account)
            }
        }
    });

});

function setupGlobalAuthHeader() {
    var header = "Bearer " + getAccessToken();
    axios.defaults.headers.common["Authorization"] = header;
    return header;
}
