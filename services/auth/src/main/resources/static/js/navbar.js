axios.get("/role")
    .then(response => {
        registerNav(response.data);

        new Vue({
            el: "#nav",
            template: "<custom-nav></custom-nav>"
        })
    });
