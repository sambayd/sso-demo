function registerNav(role) {
    Vue.component("custom-nav", {
        data: function () {
            return {
                role: role
            };
        },
        template:
            "<nav class='navbar navbar-inverse'>" +
                "<a class='navbar-brand' href='/'>SSO Demo</a>" +
                "<ul class='nav navbar-nav'>" +
                    "<li class='nav-item active'>" +
                        "<a href='/index.html'>Home</a>" +
                    "</li>" +
                    "<li class='nav-item' v-if='role === \"ANONYMOUS\"'>" +
                        "<a href='/login.html'>Log in</a>" +
                    "</li>" +
                    "<li class='nav-item' v-if='role === \"ANONYMOUS\"'>" +
                        "<a href='/registration.html' class='nav-link'>Sing up</a>" +
                    "</li>" +
                    "<li class='nav-item' v-if='role === \"NEW_USER_LOGIN\"'>" +
                        "<a href='/confirm-registration.html' class='nav-link'>Confirm registration</a>" +
                    "</li>" +
                    "<li class='nav-item' v-if='role === \"EXISTING_USER_REGISTER\"'>" +
                        "<a href='/confirm-login.html' class='nav-link'>Confirm login</a>" +
                    "</li>" +
                    "<li class='nav-item' v-if='role === \"USER\"'>" +
                        "<a href='/account.html' class='nav-link'>Account service</a>" +
                    "</li>" +
                    "<li class='nav-item' v-if='role === \"USER\"'>" +
                        "<a href='/notes.html' class='nav-link'>Note service</a>" +
                    "</li>" +
                    "<li class='nav-item' v-if='role === \"USER\"'>" +
                        "<a href='/logout.html' class='nav-link'>Log out</a>" +
                    "</li>" +
                "</ul>" +
            "</nav>"
    })
}
