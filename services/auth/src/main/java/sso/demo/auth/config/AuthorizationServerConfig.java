package sso.demo.auth.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
@EnableAuthorizationServer
@RequiredArgsConstructor
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    private final Environment env;

    private final TokenStore tokenStore;

    private final TokenEnhancer tokenEnhancer;

    private final DefaultTokenServices tokenServices;

    private final AuthenticationManager authenticationManager;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        // @formatter:off
        clients
            .inMemory()
                .withClient("external")
                .secret("{noop}password")
                .authorizedGrantTypes("authorization_code", "password", "refresh_token")
                .authorities("ROLE_EXTERNAL_CLIENT")
                .scopes("external-scope")
                .autoApprove(true)
            .and()
                .withClient("auth-service")
                .secret("{noop}" + env.getProperty("AUTH_SERVICE_SECRET"))
                .authorizedGrantTypes("client_credentials", "refresh_token")
                .authorities("ROLE_INTERNAL_CLIENT")
                .scopes("internal-scope")
            .and()
                .withClient("account-service")
                .secret("{noop}" + env.getProperty("ACCOUNT_SERVICE_SECRET"))
                .authorizedGrantTypes("client_credentials", "refresh_token")
                .authorities("ROLE_INTERNAL_CLIENT")
                .scopes("internal-scope")
            .and()
                .withClient("note-service")
                .secret("{noop}" + env.getProperty("NOTE_SERVICE_SECRET"))
                .authorizedGrantTypes("client_credentials", "refresh_token")
                .authorities("ROLE_INTERNAL_CLIENT")
                .scopes("internal-scope");
        // @formatter:on
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints.tokenStore(tokenStore)
                .authenticationManager(authenticationManager)
                .tokenEnhancer(tokenEnhancer)
                .tokenServices(tokenServices);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) {
        security.checkTokenAccess("permitAll()");
    }
}
