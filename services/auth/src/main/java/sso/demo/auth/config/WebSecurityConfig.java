package sso.demo.auth.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.web.filter.CompositeFilter;

import javax.servlet.Filter;
import java.util.Arrays;

import static sso.demo.auth.model.Authority.*;

@Configuration
@EnableOAuth2Client
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final OAuth2ClientContext oAuth2ClientContext;

    private final ExternalAuthProvidersConfig.OAuth2Properties googleOAuth2Properties;

    private final ExternalAuthProvidersConfig.OAuth2Properties facebookOAuth2Properties;

    private final AuthenticationSuccessHandler googleRegistrationOAuth2AuthenticationSuccessHandler;

    private final AuthenticationSuccessHandler facebookRegistrationOAuth2AuthenticationSuccessHandler;

    private final AuthenticationSuccessHandler googleAuthenticationOAuth2AuthenticationSuccessHandler;

    private final AuthenticationSuccessHandler facebookAuthenticationOAuth2AuthenticationSuccessHandler;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    // TODO remove it
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("123").password("{noop}user").roles("USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http
            .authorizeRequests()
                .antMatchers("/login**", "/registration**").anonymous()
                .antMatchers("/logout**").hasAnyRole("USER", "EXISTING_USER_REGISTER", "NEW_USER_LOGIN")
                .antMatchers("/confirm-login**").hasAuthority(ROLE_EXISTING_USER_REGISTER.toString())
                .antMatchers("/confirm-registration**").hasAuthority(ROLE_NEW_USER_LOGIN.toString())
                .antMatchers("/", "/index.html", "/account-service/**", "/note-service/**", "/name").permitAll()
                .antMatchers("/js/**", "/css/**", "/role").permitAll()
                .anyRequest().hasAuthority(ROLE_USER.toString())
            .and()
            .logout().logoutSuccessUrl("/")
            .and()
            .csrf().ignoringAntMatchers("/account-service/**", "/note-service/**")
                .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
            .and()
            .addFilterBefore(externalAuthFilter(), BasicAuthenticationFilter.class);
        // @formatter:on
    }

    private Filter externalAuthFilter() {
        final CompositeFilter compositeFilter = new CompositeFilter();
        compositeFilter.setFilters(Arrays.asList(
                createExternalAuthFilter("/registration/google", googleOAuth2Properties,
                        googleRegistrationOAuth2AuthenticationSuccessHandler),
                createExternalAuthFilter("/registration/facebook", facebookOAuth2Properties,
                        facebookRegistrationOAuth2AuthenticationSuccessHandler),
                createExternalAuthFilter("/login/google", googleOAuth2Properties,
                        googleAuthenticationOAuth2AuthenticationSuccessHandler),
                createExternalAuthFilter("/login/facebook", facebookOAuth2Properties,
                        facebookAuthenticationOAuth2AuthenticationSuccessHandler)));
        return compositeFilter;
    }

    private Filter createExternalAuthFilter(String contextPath,
                                            ExternalAuthProvidersConfig.OAuth2Properties oAuth2Properties,
                                            AuthenticationSuccessHandler authenticationSuccessHandler) {
        final OAuth2ClientAuthenticationProcessingFilter filter =
                new OAuth2ClientAuthenticationProcessingFilter(contextPath);
        filter.restTemplate = new OAuth2RestTemplate(oAuth2Properties.getClient(), oAuth2ClientContext);
        final UserInfoTokenServices services = new UserInfoTokenServices(oAuth2Properties.getResource().getUserInfoUri(),
                oAuth2Properties.getClient().getClientId());
        services.setRestTemplate(filter.restTemplate);
        filter.setTokenServices(services);
        filter.setAuthenticationSuccessHandler(authenticationSuccessHandler);
        return filter;
    }

    @Bean
    public FilterRegistrationBean<OAuth2ClientContextFilter> filterRegistrationBean(
            OAuth2ClientContextFilter oAuth2ClientContextFilter) {
        final FilterRegistrationBean<OAuth2ClientContextFilter> bean = new FilterRegistrationBean<>();
        bean.setFilter(oAuth2ClientContextFilter);
        bean.setOrder(-100);
        return bean;
    }
}
