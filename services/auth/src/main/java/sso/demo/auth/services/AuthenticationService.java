package sso.demo.auth.services;

import sso.demo.auth.model.Authority;

import java.util.List;

public interface AuthenticationService {

    void updateAuthorities(List<Authority> authorities);
}
