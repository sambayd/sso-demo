package sso.demo.auth.controllers;

import sso.demo.auth.exceptions.AccountServiceNotAvailableException;
import sso.demo.auth.model.CustomAuthentication;
import sso.demo.auth.model.enitites.User;
import sso.demo.auth.services.AuthenticationService;
import sso.demo.auth.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import sso.demo.auth.model.Authority;

import java.security.Principal;

import static java.util.Collections.singletonList;

@RestController
@RequiredArgsConstructor
public class AuthController {

    private final AuthenticationService authenticationService;

    private final UserService userService;

    @GetMapping("/principal")
    public Principal principal(Principal principal) {
        return principal;
    }

    @GetMapping("/name")
    public String name(OAuth2Authentication principal) {
        final User user = ((CustomAuthentication) principal.getUserAuthentication()).getUser();
        return user.getFirstName() + " " + user.getLastName();
    }

    @GetMapping("/role")
    public String role(OAuth2Authentication principal) {
        if (principal == null) return "ANONYMOUS";
        return ((CustomAuthentication) principal.getUserAuthentication()).getCustomAuthorities().get(0)
                .toString().replace("ROLE_", "");
    }

    @PostMapping("/confirm-login")
    @Secured("ROLE_EXISTING_USER_REGISTER")
    public ResponseEntity<?> approveLogin() {
        authenticationService.updateAuthorities(singletonList(Authority.ROLE_USER));
        return ResponseEntity.ok().build();
    }

    @PostMapping("/confirm-registration")
    @Secured("ROLE_NEW_USER_LOGIN")
    public ResponseEntity<?> approveRegistration(OAuth2Authentication oAuth2Authentication) {
        if (!(oAuth2Authentication.getUserAuthentication() instanceof CustomAuthentication))
            throw new IllegalStateException(new ClassCastException("cannot cast Authentication to CustomAuthentication"));
        try {
            userService.registerNewUser(((CustomAuthentication) oAuth2Authentication.getUserAuthentication()).getUser());
        } catch (AccountServiceNotAvailableException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        authenticationService.updateAuthorities(singletonList(Authority.ROLE_USER));
        return ResponseEntity.ok().build();
    }
}
