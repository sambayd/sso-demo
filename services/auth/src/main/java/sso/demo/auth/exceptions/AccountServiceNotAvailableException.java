package sso.demo.auth.exceptions;

public class AccountServiceNotAvailableException extends Exception {

    public AccountServiceNotAvailableException(Throwable cause) {
        super(cause);
    }
}
