package sso.demo.auth.filters.handlers;

import sso.demo.auth.services.OAuth2DataExtractorService;
import sso.demo.auth.services.UserService;
import sso.demo.auth.model.Authority;
import sso.demo.auth.model.CustomAuthentication;
import sso.demo.auth.model.ExternalAuthProvider;
import sso.demo.auth.model.enitites.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

@RequiredArgsConstructor
public class AuthenticationOAuth2AuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private final UserService userService;

    private final ExternalAuthProvider externalAuthProvider;

    private final OAuth2DataExtractorService oAuth2DataExtractorService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws ServletException, IOException {
        if (!(authentication instanceof OAuth2Authentication))
            throw new IllegalStateException(new ClassCastException("cannot cast Authentication to OAuth2Authentication"));

        final OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) authentication;
        final String externalAuthProviderId = oAuth2Authentication.getUserAuthentication().getPrincipal().toString();

        final User existingUser =
                userService.getUserByExternalPrincipal(externalAuthProvider, externalAuthProviderId);

        if (existingUser != null) {
            final OAuth2Authentication newAuthentication = new OAuth2Authentication(oAuth2Authentication.getOAuth2Request(),
                    new CustomAuthentication(existingUser, Collections.singletonList(Authority.ROLE_USER)));
            SecurityContextHolder.getContext().setAuthentication(newAuthentication);

            super.onAuthenticationSuccess(request, response, newAuthentication);
        } else {
            @SuppressWarnings("unchecked") final Map<String, String> details =
                    (Map<String, String>) oAuth2Authentication.getUserAuthentication().getDetails();

            final User user = User.builder()
                    .externalAuthProvider(externalAuthProvider)
                    .externalAuthProviderId(externalAuthProviderId)
                    .firstName(oAuth2DataExtractorService.getFirstName(details))
                    .lastName(oAuth2DataExtractorService.getLastName(details))
                    .build();

            final OAuth2Authentication newAuthentication = new OAuth2Authentication(oAuth2Authentication.getOAuth2Request(),
                    new CustomAuthentication(user, Collections.singletonList(Authority.ROLE_NEW_USER_LOGIN)));
            SecurityContextHolder.getContext().setAuthentication(newAuthentication);

            response.sendRedirect("/confirm-registration.html");
        }
    }
}
