package sso.demo.auth.data.jpa;

import sso.demo.auth.model.ExternalAuthProvider;
import sso.demo.auth.model.enitites.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

    User findByExternalAuthProviderAndExternalAuthProviderId(ExternalAuthProvider externalAuthProvider,
                                                             String externalAuthProviderId);
}
