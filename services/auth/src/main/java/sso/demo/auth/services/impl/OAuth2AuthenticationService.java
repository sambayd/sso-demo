package sso.demo.auth.services.impl;

import sso.demo.auth.model.Authority;
import sso.demo.auth.model.CustomAuthentication;
import sso.demo.auth.services.AuthenticationService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OAuth2AuthenticationService implements AuthenticationService {

    @Override
    public void updateAuthorities(List<Authority> authorities) {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();

        if (!(authentication instanceof OAuth2Authentication))
            throw new IllegalStateException(new ClassCastException("cannot cast Authentication to OAuth2Authentication"));

        final OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) authentication;

        if (!(oAuth2Authentication.getUserAuthentication() instanceof CustomAuthentication))
            throw new IllegalStateException(new ClassCastException("cannot cast Authentication to CustomAuthentication"));

        final CustomAuthentication customAuthentication =
                (CustomAuthentication) oAuth2Authentication.getUserAuthentication();

        securityContext.setAuthentication(new OAuth2Authentication(oAuth2Authentication.getOAuth2Request(),
                new CustomAuthentication(customAuthentication.getUser(), authorities)));
    }
}
