package sso.demo.auth.services.impl;

import sso.demo.auth.services.OAuth2DataExtractorService;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class OAuth2FacebookDataExtractorService implements OAuth2DataExtractorService {

    @Override
    public String getFirstName(Map<String, String> details) {
        return details.get("name").split(" ")[0];
    }

    @Override
    public String getLastName(Map<String, String> details) {
        return details.get("name").split(" ")[1];
    }
}
