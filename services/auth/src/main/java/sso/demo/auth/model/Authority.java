package sso.demo.auth.model;

public enum Authority {
    ROLE_EXISTING_USER_REGISTER,
    ROLE_NEW_USER_LOGIN,
    ROLE_USER
}
