package sso.demo.auth.config;

import sso.demo.auth.filters.handlers.AuthenticationOAuth2AuthenticationSuccessHandler;
import sso.demo.auth.filters.handlers.RegistrationOAuth2AuthenticationSuccessHandler;
import sso.demo.auth.model.ExternalAuthProvider;
import sso.demo.auth.services.UserService;
import sso.demo.auth.services.impl.OAuth2FacebookDataExtractorService;
import sso.demo.auth.services.impl.OAuth2GoogleDataExtractorService;
import lombok.Getter;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;

@Configuration
public class ExternalAuthProvidersConfig {

    @Getter
    public static class OAuth2Properties {

        @NestedConfigurationProperty
        private AuthorizationCodeResourceDetails client = new AuthorizationCodeResourceDetails();

        @NestedConfigurationProperty
        private ResourceServerProperties resource = new ResourceServerProperties();
    }

    @Bean
    @ConfigurationProperties("google")
    public OAuth2Properties googleOAuth2Properties() {
        return new OAuth2Properties();
    }

    @Bean
    @ConfigurationProperties("facebook")
    public OAuth2Properties facebookOAuth2Properties() {
        return new OAuth2Properties();
    }

    @Bean
    public RegistrationOAuth2AuthenticationSuccessHandler googleRegistrationOAuth2AuthenticationSuccessHandler(
            UserService userService, OAuth2GoogleDataExtractorService oAuth2GoogleDataExtractorService) {
        return new RegistrationOAuth2AuthenticationSuccessHandler(userService, ExternalAuthProvider.GOOGLE,
                oAuth2GoogleDataExtractorService);
    }

    @Bean
    public RegistrationOAuth2AuthenticationSuccessHandler facebookRegistrationOAuth2AuthenticationSuccessHandler(
            UserService userService, OAuth2FacebookDataExtractorService oAuth2FacebookDataExtractorService) {
        return new RegistrationOAuth2AuthenticationSuccessHandler(userService, ExternalAuthProvider.FACEBOOK,
                oAuth2FacebookDataExtractorService);
    }

    @Bean
    public AuthenticationOAuth2AuthenticationSuccessHandler googleAuthenticationOAuth2AuthenticationSuccessHandler(
            UserService userService, OAuth2GoogleDataExtractorService googleDataExtractorService) {
        return new AuthenticationOAuth2AuthenticationSuccessHandler(userService, ExternalAuthProvider.GOOGLE,
                googleDataExtractorService);
    }

    @Bean
    public AuthenticationOAuth2AuthenticationSuccessHandler facebookAuthenticationOAuth2AuthenticationSuccessHandler(
            UserService userService, OAuth2FacebookDataExtractorService oAuth2FacebookDataExtractorService) {
        return new AuthenticationOAuth2AuthenticationSuccessHandler(userService, ExternalAuthProvider.FACEBOOK,
                oAuth2FacebookDataExtractorService);
    }


    @Bean
    @ConfigurationProperties("custom.security.oauth2.client")
    public ClientCredentialsResourceDetails clientCredentialsResourceDetails() {
        return new ClientCredentialsResourceDetails();
    }

    @Bean
    public OAuth2RestTemplate oAuth2RestTemplate(ClientCredentialsResourceDetails clientCredentialsResourceDetails) {
        return new OAuth2RestTemplate(clientCredentialsResourceDetails);
    }
}
