package sso.demo.auth.services;

import sso.demo.auth.exceptions.AccountServiceNotAvailableException;
import sso.demo.auth.exceptions.UserAlreadyRegisteredException;
import sso.demo.auth.model.ExternalAuthProvider;
import sso.demo.auth.model.enitites.User;

public interface UserService {

    User getUserByExternalPrincipal(ExternalAuthProvider externalAuthProvider, String externalAuthProviderId);

    User registerNewUser(User user) throws UserAlreadyRegisteredException, AccountServiceNotAvailableException;
}
