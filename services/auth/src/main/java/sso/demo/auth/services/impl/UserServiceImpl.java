package sso.demo.auth.services.impl;

import sso.demo.auth.data.jpa.UserRepository;
import sso.demo.auth.exceptions.AccountServiceNotAvailableException;
import sso.demo.auth.exceptions.UserAlreadyRegisteredException;
import sso.demo.auth.model.Account;
import sso.demo.auth.model.ExternalAuthProvider;
import sso.demo.auth.model.enitites.User;
import sso.demo.auth.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResourceAccessException;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final OAuth2RestTemplate oAuth2RestTemplate;

    @Value("${account-service.host}")
    private String accountServiceHost;

    @Value("${account-service.port}")
    private String accountServicePort;

    @Override
    public User getUserByExternalPrincipal(ExternalAuthProvider externalAuthProvider,
                                           String externalAuthProviderId) {
        return userRepository.findByExternalAuthProviderAndExternalAuthProviderId(externalAuthProvider,
                externalAuthProviderId);
    }

    @Override
    @Transactional(rollbackFor = AccountServiceNotAvailableException.class)
    public User registerNewUser(User user) throws AccountServiceNotAvailableException {
        final User existingUser = userRepository.findByExternalAuthProviderAndExternalAuthProviderId(
                user.getExternalAuthProvider(), user.getExternalAuthProviderId());

        if (existingUser != null)
            throw new UserAlreadyRegisteredException();

        final User savedUser = userRepository.save(user);

        // TODO replace it with FeignClient
        try {
            oAuth2RestTemplate.postForLocation("http://" + accountServiceHost + ":" + accountServicePort +
                            "/account-service/api/internal/account",
                    Account.builder()
                            .id(savedUser.getId())
                            .firstName(user.getFirstName())
                            .lastName(user.getLastName())
                            .build());

        } catch (ResourceAccessException e) {
            throw new AccountServiceNotAvailableException(e);
        }

        return savedUser;
    }
}
