package sso.demo.auth.services;

import java.util.Map;

public interface OAuth2DataExtractorService {

    String getFirstName(Map<String, String> details);

    String getLastName(Map<String, String> details);
}
