package sso.demo.auth.model;

import sso.demo.auth.model.enitites.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public final class CustomAuthentication implements Authentication {

    @Getter
    private final User user;

    @Getter
    private final List<? extends Authority> customAuthorities;

    @Getter
    private final Collection<? extends GrantedAuthority> authorities;

    @Getter
    @Setter
    private boolean authenticated = true;

    public CustomAuthentication(User user, List<Authority> authorities) {
        this.user = user;
        this.customAuthorities = authorities;
        this.authorities = transformAuthority(authorities);
    }

    private Collection<GrantedAuthority> transformAuthority(Collection<Authority> authorities) {
        return authorities
                .stream()
                .map(authority -> new SimpleGrantedAuthority(authority.toString()))
                .collect(Collectors.toList());
    }

    @Override
    public Long getPrincipal() {
        return user.getId();
    }

    @Override
    public String getName() {
        return String.valueOf(user.getId());
    }

    @Override
    public Object getCredentials() {
        return user.getPassword();
    }

    @Override
    public Object getDetails() {
        return null;
    }
}
