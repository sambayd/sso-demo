package sso.demo.auth.model;

public enum ExternalAuthProvider {
    GOOGLE, FACEBOOK
}
