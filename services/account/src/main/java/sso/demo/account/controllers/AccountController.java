package sso.demo.account.controllers;

import sso.demo.account.exceptions.AccountAlreadyRegisteredException;
import sso.demo.account.exceptions.AccountDoestNotExistsException;
import sso.demo.account.model.entities.Account;
import sso.demo.account.services.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.*;

@RestController
@RequestMapping("/api/internal/account")
@RequiredArgsConstructor
@PreAuthorize("hasRole('INTERNAL_CLIENT')")
public class AccountController {

    private final AccountService accountService;

    @PostMapping
    public ResponseEntity<Long> createAccount(@RequestBody Account account) {
        try {
            return ok(accountService.createAccount(account));
        } catch (AccountAlreadyRegisteredException e) {
            return badRequest().build();
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<Account> getAccount(@PathVariable("id") long id) {
        try {
            return ok(accountService.getAccount(id));
        } catch (AccountDoestNotExistsException e) {
            return notFound().build();
        }
    }
}
