package sso.demo.account.data.jpa;

import sso.demo.account.model.entities.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Long> {
}
