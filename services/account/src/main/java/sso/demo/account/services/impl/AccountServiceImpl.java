package sso.demo.account.services.impl;

import sso.demo.account.data.jpa.AccountRepository;
import sso.demo.account.exceptions.AccountAlreadyRegisteredException;
import sso.demo.account.exceptions.AccountDoestNotExistsException;
import sso.demo.account.model.entities.Account;
import sso.demo.account.services.AccountService;
import sso.demo.lib.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    private final UserService userService;

    @Override
    public long createAccount(Account account) throws AccountAlreadyRegisteredException {
        if (accountRepository.existsById(account.getId()))
            throw new AccountAlreadyRegisteredException();
        return accountRepository.save(account).getId();
    }

    @Override
    public long createAccountForCurrentUser(Account account) throws AccountAlreadyRegisteredException {
        account.setId(userService.getUserId());
        if (accountRepository.existsById(account.getId()))
            throw new AccountAlreadyRegisteredException();

        return accountRepository.save(account).getId();
    }

    @Override
    public Account getAccount(long id) throws AccountDoestNotExistsException {
        return accountRepository.findById(id).orElseThrow(AccountDoestNotExistsException::new);
    }

    @Override
    public Account getCurrentUserAccount() throws AccountDoestNotExistsException {
        return accountRepository.findById(userService.getUserId()).orElseThrow(AccountDoestNotExistsException::new);
    }

    @Override
    public void updateCurrentUserAccount(Account account) throws AccountDoestNotExistsException {
        account.setId(userService.getUserId());
        if (!accountRepository.existsById(account.getId()))
            throw new AccountDoestNotExistsException();

        accountRepository.save(account);
    }

    @Override
    public void deleteCurrentUserAccount() throws AccountDoestNotExistsException {
        final long id = userService.getUserId();
        if (!accountRepository.existsById(id))
            throw new AccountDoestNotExistsException();
        accountRepository.deleteById(id);
    }
}
