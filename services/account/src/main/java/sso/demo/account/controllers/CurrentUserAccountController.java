package sso.demo.account.controllers;

import sso.demo.account.exceptions.AccountDoestNotExistsException;
import sso.demo.account.model.entities.Account;
import sso.demo.account.services.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/account/current")
@RequiredArgsConstructor
public class CurrentUserAccountController {

    private final AccountService accountService;

    /*@PostMapping
    public ResponseEntity<Long> createAccount(@RequestBody Account account) {
        try {
            return ok(accountService.createAccountForCurrentUser(account));
        } catch (AccountAlreadyRegisteredException e) {
            return badRequest().build();
        }
    }*/

    @GetMapping
    public ResponseEntity<Account> getAccount() {
        try {
            return ok(accountService.getCurrentUserAccount());
        } catch (AccountDoestNotExistsException e) {
            return notFound().build();
        }
    }

    @PutMapping
    public ResponseEntity<?> updateAccount(@RequestBody Account account) {
        try {
            accountService.updateCurrentUserAccount(account);
            return ok().build();
        } catch (AccountDoestNotExistsException e) {
            return notFound().build();
        }
    }

    @DeleteMapping
    public ResponseEntity<?> deleteAccount() {
        try {
            accountService.deleteCurrentUserAccount();
            return ok().build();
        } catch (AccountDoestNotExistsException e) {
            return notFound().build();
        }
    }
}
