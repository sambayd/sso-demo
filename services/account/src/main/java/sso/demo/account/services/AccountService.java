package sso.demo.account.services;

import sso.demo.account.exceptions.AccountAlreadyRegisteredException;
import sso.demo.account.exceptions.AccountDoestNotExistsException;
import sso.demo.account.model.entities.Account;

public interface AccountService {

    long createAccount(Account account) throws AccountAlreadyRegisteredException;

    long createAccountForCurrentUser(Account account) throws AccountAlreadyRegisteredException;

    Account getAccount(long id) throws AccountDoestNotExistsException;

    Account getCurrentUserAccount() throws AccountDoestNotExistsException;

    void updateCurrentUserAccount(Account account) throws AccountDoestNotExistsException;

    void deleteCurrentUserAccount() throws AccountDoestNotExistsException;
}
