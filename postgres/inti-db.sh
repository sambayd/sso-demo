#!/bin/bash

psql --set=auth_password=$AUTH_PASSWORD \
    --set=account_password=$ACCOUNT_PASSWORD \
    --set=note_password=$NOTE_PASSWORD \
    $POSTGRES_USER $POSTGRES_DB <<-EOSQL
    CREATE DATABASE auth;
    CREATE USER auth WITH password :'auth_password';
    GRANT ALL ON auth TO auth;

    CREATE DATABASE account;
    CREATE USER account WITH password :'account_password';
    GRANT ALL ON account TO account;

    CREATE DATABASE note;
    CREATE USER note WITH password :'note_password';
    GRANT ALL ON note TO note;
EOSQL
